package org.lesson.meeting3;

public class EmployeeNotFoundException extends RuntimeException {
	
	EmployeeNotFoundException(Long id) {
		super("Could not find employee " + id);
	}
}

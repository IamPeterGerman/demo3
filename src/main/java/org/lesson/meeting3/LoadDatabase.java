package org.lesson.meeting3;
//package org.lesson.meeting3.payroll;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class LoadDatabase {
//	
//	@Bean
//	CommandLineRunner initDatabase(EmployeeRepository repository) {
//		return args -> {
//			log.info("Preloading " + repository.save(new Employee("Bilbo Baggins", "burglar")));
//			log.info("Preloading " + repository.save(new Employee("Frodo Baggins", "thief")));
//		};
//	}
//}

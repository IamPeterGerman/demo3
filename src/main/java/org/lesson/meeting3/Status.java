package org.lesson.meeting3;

public enum Status {
	IN_PROGRESS,
	COMPLETED,
	CANCELLED;
}
